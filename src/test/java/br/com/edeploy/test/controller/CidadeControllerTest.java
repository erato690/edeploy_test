package br.com.edeploy.test.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import br.com.edeploy.test.bussines.CidadeBussines;
import br.com.edeploy.test.model.AjaxResponseBody;
import br.com.edeploy.test.model.CidadeEstado;
import br.com.edeploy.test.service.CidadeService;

@RunWith(PowerMockRunner.class)
public class CidadeControllerTest {

	
	@Mock
	private CidadeService cidadeService;
	@Mock
	private CidadeBussines cidadeBussines;
	private ArrayList<CidadeEstado> listaCidadeEstado;
	private ArrayList<CidadeEstado> listaEstadosFiltrado ;
	
	@InjectMocks
	private CidadeController cidadeController;

	

	
	
	@Test
	public void  navegacaoIndex() throws Exception{
		
		
		
		ModelMap model = new ModelMap();
		BindingResult result =   Mockito.mock(BindingResult.class);
		
		
		Whitebox.setInternalState(cidadeController, "cidadeService", cidadeService);
		Whitebox.setInternalState(cidadeController, "cidadeBussines", cidadeBussines);
		
		when(cidadeService.obterTodasCidades()).thenReturn(listaCidadeEstado);
		
		
		CidadeEstado cidadePesquisa = new CidadeEstado();
		cidadePesquisa.setEstado("São Paulo");
		cidadePesquisa.setNome("Barão");	
		
		Assert.assertEquals("deu erro não foi para a index:","index" ,cidadeController.pesquisaCidade(cidadePesquisa, model, result));
          
		
	}
	
	@Test
	public void  adicionouListadeCidade() throws Exception{
		
		
		
		ModelMap model = new ModelMap();
		BindingResult result =   Mockito.mock(BindingResult.class);
		
		
		Whitebox.setInternalState(cidadeController, "cidadeService", cidadeService);
		Whitebox.setInternalState(cidadeController, "cidadeBussines", cidadeBussines);
		
		when(cidadeService.obterTodasCidades()).thenReturn(listaCidadeEstado);
		 
		when(cidadeBussines.obterCidadeFitro(Matchers.anyList(), Matchers.any())).thenReturn(this.listaEstadosFiltrado);
			
		
		
		CidadeEstado cidadePesquisa = new CidadeEstado();
		cidadePesquisa.setEstado("São Paulo");
		cidadePesquisa.setNome("Barão");	
		
		cidadeController.pesquisaCidade(cidadePesquisa, model, result);
		
		
		//Assert.assertNotNull( model.get("listCidade"));
		Assert.assertEquals("Não tornou nenhuma cidade",3,((List) model.get("listCidade")).size());
          
		
	}
	
	@Test
	public void  pesquisaListadeCidadeInvalida() throws Exception{
		
		
		
		ModelMap model = new ModelMap();
		BindingResult result =   Mockito.mock(BindingResult.class);
		
		
		Whitebox.setInternalState(cidadeController, "cidadeService", cidadeService);
		Whitebox.setInternalState(cidadeController, "cidadeBussines", cidadeBussines);
		
		when(cidadeService.obterTodasCidades()).thenReturn(listaCidadeEstado);
		 
		when(cidadeBussines.obterCidadeFitro(Matchers.anyList(), Matchers.any())).thenReturn(new ArrayList<>());
			
		
		
		CidadeEstado cidadePesquisa = new CidadeEstado();
		cidadePesquisa.setEstado("São Paulo");
		cidadePesquisa.setNome("tsadas");	
		
		cidadeController.pesquisaCidade(cidadePesquisa, model, result);
		
		
		//Assert.assertNotNull( model.get("listCidade"));
		Assert.assertEquals("Não tornou nenhuma cidade",0,((List) model.get("listCidade")).size());
          
		
	}
	
	@Test
	public void  verPontuacaocidade() throws Exception{
		
		
		
		ModelMap model = new ModelMap();
		BindingResult result =   Mockito.mock(BindingResult.class);
		
		
		Whitebox.setInternalState(cidadeController, "cidadeService", cidadeService);
		Whitebox.setInternalState(cidadeController, "cidadeBussines", cidadeBussines);
		
		when(cidadeService.obterPontosCidade(Matchers.any())).thenReturn(10003D);
		
		
		CidadeEstado cidadePesquisa = new CidadeEstado();
		cidadePesquisa.setEstado("São Paulo");
		cidadePesquisa.setNome("bar");	
		
		AjaxResponseBody  ajaxBody= cidadeController.buscarPontocaoCidade(cidadePesquisa);
		
		
		//Assert.assertNotNull( model.get("listCidade"));
		Assert.assertEquals("erro no retorno do ajax",ajaxBody.getStatus(),"200");
		Assert.assertEquals("erro no retorno do ajax",ajaxBody.getMensagem(),null);
		Assert.assertEquals("erro no retorno do ajax",ajaxBody.getResult(),10003D);
          
		
	}
	

	@Test
	public void  erroVerPontuacaocidade() throws Exception{
		
		
		
		ModelMap model = new ModelMap();
		BindingResult result =   Mockito.mock(BindingResult.class);
		
		
		Whitebox.setInternalState(cidadeController, "cidadeService", cidadeService);
		Whitebox.setInternalState(cidadeController, "cidadeBussines", cidadeBussines);
		
		when(cidadeService.obterPontosCidade(Matchers.any())).thenThrow(new RuntimeException("Erro de comunicacao"));
		
		
		CidadeEstado cidadePesquisa = new CidadeEstado();
		cidadePesquisa.setEstado("São Paulo");
		cidadePesquisa.setNome("bar");	
		
		AjaxResponseBody  ajaxBody= cidadeController.buscarPontocaoCidade(cidadePesquisa);
		
		
		//Assert.assertNotNull( model.get("listCidade"));
		Assert.assertEquals("erro no retorno do ajax",ajaxBody.getStatus(),"500");
		Assert.assertEquals("erro no retorno do ajax",ajaxBody.getMensagem(),"Erro de comunicacao");
		Assert.assertEquals("erro no retorno do ajax",ajaxBody.getResult(),null);
          
		
	}
	
	
	@Before
	public void init(){
		
		MockitoAnnotations.initMocks(this);
		
		listaCidadeEstado = new ArrayList<CidadeEstado>();
		

		
		listaEstadosFiltrado = new ArrayList<>();
		
		CidadeEstado cidade1= new CidadeEstado();
		cidade1.setNome("Barão Ataliba Nogueira");
		cidade1.setEstado("São Pualo");
		
	
		CidadeEstado cidade2= new CidadeEstado();
		cidade2.setNome("Barão de Antonina");
		cidade2.setEstado("São Pualo");
		
		
		CidadeEstado cidade3= new CidadeEstado();
		cidade3.setNome("Cabralia Paulista");
		cidade3.setEstado("São Pualo");
		
		CidadeEstado cidade4= new CidadeEstado();
		cidade4.setNome("Cachoeira Paulista");
		cidade4.setEstado("São Pualo");
		
	
		CidadeEstado cidade5= new CidadeEstado();
		cidade5.setNome("Cachoeira de Emas");
		cidade5.setEstado("São Pualo");
			
		CidadeEstado cidade6= new CidadeEstado();
		cidade6.setNome("Barão Ataliba jau");
		cidade6.setEstado("São Pualo");

		
		listaCidadeEstado.add(cidade1);
		listaCidadeEstado.add(cidade2);
		listaCidadeEstado.add(cidade3);
		listaCidadeEstado.add(cidade4);
		listaCidadeEstado.add(cidade5);
		listaCidadeEstado.add(cidade6);
		
		listaEstadosFiltrado.add(cidade1);
		listaEstadosFiltrado.add(cidade2);
		listaEstadosFiltrado.add(cidade3);
		
	}
}
