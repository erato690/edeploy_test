package br.com.edeploy.test.bussines;




import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.edeploy.test.model.CidadeEstado;

@RunWith(PowerMockRunner.class)
public class CidadeBussinesTest {

	
	private List<CidadeEstado> listaCidadeEstado =  null;
	

	
	@Test
	public void filtroLista(){
		
		CidadeBussines cidade = new CidadeBussines();
		
		CidadeEstado cidadePesquisa1 = new CidadeEstado();
		cidadePesquisa1.setNome("Bar");
		cidadePesquisa1.setEstado("São Pualo");
		
		Assert.assertEquals("Erro no filtro era para trazer 3 item ", 3, cidade.obterCidadeFitro(this.listaCidadeEstado, cidadePesquisa1).size());
		
		
		cidadePesquisa1.setNome("Cabralia");	
		Assert.assertEquals("Erro no filtro era para trazer 1 item", 1, cidade.obterCidadeFitro(this.listaCidadeEstado, cidadePesquisa1).size());
		
		cidadePesquisa1.setNome("Cachoeira");
		Assert.assertEquals("Erro no filtro era para trazer 2 item ", 2, cidade.obterCidadeFitro(this.listaCidadeEstado, cidadePesquisa1).size());
		
		
		cidadePesquisa1.setNome("Barão Ataliba");
		Assert.assertEquals("Erro no filtro era para trazer 2 item", 2, cidade.obterCidadeFitro(this.listaCidadeEstado, cidadePesquisa1).size());
		
		
		cidadePesquisa1.setNome("Barão Ataliba Nogueira");
		Assert.assertEquals("Erro no filtro era para trazer 1 item", 1, cidade.obterCidadeFitro(this.listaCidadeEstado, cidadePesquisa1).size());
		
		
		cidadePesquisa1.setNome("Zere");
		Assert.assertEquals("Erro no filtro era para trazer 1 item", 0, cidade.obterCidadeFitro(this.listaCidadeEstado, cidadePesquisa1).size());
		
		
	}
	
	@Before
	public void init(){
		
		listaCidadeEstado = new ArrayList<CidadeEstado>();
		
		CidadeEstado cidade1= new CidadeEstado();
		cidade1.setNome("Barão Ataliba Nogueira");
		cidade1.setEstado("São Pualo");
		
	
		CidadeEstado cidade2= new CidadeEstado();
		cidade2.setNome("Barão de Antonina");
		cidade2.setEstado("São Pualo");
		
		
		CidadeEstado cidade3= new CidadeEstado();
		cidade3.setNome("Cabralia Paulista");
		cidade3.setEstado("São Pualo");
		
		CidadeEstado cidade4= new CidadeEstado();
		cidade4.setNome("Cachoeira Paulista");
		cidade4.setEstado("São Pualo");
		
	
		CidadeEstado cidade5= new CidadeEstado();
		cidade5.setNome("Cachoeira de Emas");
		cidade5.setEstado("São Pualo");
			
		CidadeEstado cidade6= new CidadeEstado();
		cidade6.setNome("Barão Ataliba jau");
		cidade6.setEstado("São Pualo");

		
		listaCidadeEstado.add(cidade1);
		listaCidadeEstado.add(cidade2);
		listaCidadeEstado.add(cidade3);
		listaCidadeEstado.add(cidade4);
		listaCidadeEstado.add(cidade5);
		listaCidadeEstado.add(cidade6);
	 
		
	}
}
