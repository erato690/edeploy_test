<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html>
<head>
<title>Getting Started: Serving Web Content</title>

<meta charset="UTF-8" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous" />

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
	
	<script type="text/javascript">

function verificarPontuacao(estado,cidade) {

	var data = {
		"Nome": cidade,
		"Estado":estado
	}

	$.ajax({
		type : "post",
		url : "${pageContext.request.contextPath}/buscarPontuacao",
		timeout : 100000,
		data: JSON.stringify(data),
		contentType: 'application/json; charset=utf-8',
		success : function(data) {
			console.log("SUCCESS: ", data);
			
			alert("A pontuação da cidade  de "+ cidade + " é " + " " +data.result);
		},
		error : function(e) {
			alert("Erro ao carregar a pontuação.");
		}
	});
}
</script>

</head>

<body>
	<div class="container">


 		<c:if test="${error != null}">
			<div class="alert alert-danger">
				<strong>Erro!${error}</strong> 
			</div>
		</c:if>

		<div class="row form-group" >
			<form method="get" action="/pesquisarCidade" class="form-inline">
				<div class="form-group">
					<label for="cidade">Cidade: </label> <input name="nome" type="text"
						class="form-control" />
				</div>
				<div class="form-group">
					<label for="Estado">Estado: </label> <input name="estado"
						type="text" class="form-control" />

				</div>
				<button type="submit" class="btn btn-default">Buscar</button>
			</form>
		</div>

		<div class="row form-group">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Estado</th>
						<th>Cidade</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="listValue" items="${listCidade}">
						<tr>
							<td>${listValue.estado}</td>
							<td>${listValue.nome}</td>
							<td><a
								onclick="verificarPontuacao('${listValue.estado}',' ${listValue.nome}')">Ver
									Pontuação</a></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
</body>




</html>