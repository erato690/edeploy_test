package br.com.edeploy.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.edeploy.test.bussines.CidadeBussines;
import br.com.edeploy.test.model.AjaxResponseBody;
import br.com.edeploy.test.model.CidadeEstado;
import br.com.edeploy.test.service.CidadeService;

@Controller
public class CidadeController {

	@Autowired
	private CidadeService cidadeService;

	@Autowired
	private CidadeBussines cidadeBussines;

	@RequestMapping("/")
	public String home() {

		return "index";
	}

	@RequestMapping(value = "/pesquisarCidade", method = { RequestMethod.GET })
	public String pesquisaCidade( @ModelAttribute CidadeEstado cidadeEstado, ModelMap model, BindingResult result) {

		
		List<CidadeEstado> cidadeFiltrada = null;
		try{
			List<CidadeEstado> todasCidade = cidadeService.obterTodasCidades();
			cidadeFiltrada = cidadeBussines.obterCidadeFitro(todasCidade, cidadeEstado);
		}catch (Exception e) {
			model.addAttribute("error", "Erro na comunicação com o servidor, por favor tente mais tarde.");
		}
		model.addAttribute("listCidade", cidadeFiltrada);

		return "index";
	}

	@ResponseBody
	@RequestMapping(value = "/buscarPontuacao", method = { RequestMethod.POST },consumes={"application/json"})
	public AjaxResponseBody buscarPontocaoCidade(@RequestBody CidadeEstado cidadePonto) {

		AjaxResponseBody result = new AjaxResponseBody();


		try {
			Double pontuação = this.cidadeService.obterPontosCidade(cidadePonto);
			if (pontuação != null) {
				result.setResult(pontuação.doubleValue());
				result.setStatus("200");
			} else {
				result.setStatus("200");
				result.setResult("Erro ao carregar informações");
			}
		} catch (Exception e) {
			result.setStatus("500");
			result.setMensagem(e.getMessage());
		}
		return result;
	}

	public CidadeService getCidadeService() {
		return cidadeService;
	}

	public void setCidadeService(CidadeService cidadeService) {
		this.cidadeService = cidadeService;
	}

	public CidadeBussines getCidadeBussines() {
		return cidadeBussines;
	}

	public void setCidadeBussines(CidadeBussines cidadeBussines) {
		this.cidadeBussines = cidadeBussines;
	}

}
