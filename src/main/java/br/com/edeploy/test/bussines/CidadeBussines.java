package br.com.edeploy.test.bussines;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.edeploy.test.model.CidadeEstado;

@Component
public class CidadeBussines {

	public List<CidadeEstado>  obterCidadeFitro(List<CidadeEstado> listaCidade,CidadeEstado cidadePesquisa) {

		List<CidadeEstado> cidadeFiltradas = null;
		
	
		if (listaCidade != null && !listaCidade.isEmpty()) {

			cidadeFiltradas = listaCidade.stream()
					.filter(cidadeMap -> cidadePesquisa.getEstado().toUpperCase().equals(cidadeMap.getEstado().toUpperCase()))
					.filter(cidadeMap -> cidadeMap.getNome().toUpperCase().startsWith(cidadePesquisa.getNome().toUpperCase()))
					.collect(Collectors.toList());

		}
		
		return cidadeFiltradas;

	}
}
