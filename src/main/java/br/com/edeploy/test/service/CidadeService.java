package br.com.edeploy.test.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.edeploy.test.model.CidadeEstado;

@Service
public class CidadeService {
	

	public List<CidadeEstado> obterTodasCidades(){
		
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<CidadeEstado[]> cidadeEstadoResponse =
		        restTemplate.getForEntity("http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/BuscaTodasCidades", CidadeEstado[].class);
		
		
		if(cidadeEstadoResponse.getStatusCode() == HttpStatus.OK){
			return Arrays.asList(cidadeEstadoResponse.getBody());
		}
		
		return null;

	}
	
	
	public Double obterPontosCidade(CidadeEstado cidade){
	
	     RestTemplate restTemplate = new RestTemplate();
	     restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	     ResponseEntity<Double> cidadeEstadoResponse = restTemplate.postForEntity("http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/BuscaPontos", cidade, Double.class);
	     
	     if(cidadeEstadoResponse.getStatusCode() == HttpStatus.OK){
	    	 
	    	 return cidadeEstadoResponse.getBody();
	     }
	    
		return null;

	}
}
