package br.com.edeploy.test.model;

import java.io.Serializable;

public class AjaxResponseBody implements Serializable {

	private String status;
	
	private String mensagem;
	
	private Object result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
	
	
	
}
