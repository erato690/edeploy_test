package br.com.edeploy.test.model;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "Nome", "estado" })
public class CidadeEstado implements Serializable {
	
	@NotBlank(message="Nome da cidade é necessario")
	@JsonProperty("Nome")
	private String nome;
	
	@NotBlank(message="Nome do estado é necessario")
	@JsonProperty("Estado")
	private String estado;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	

	
}
