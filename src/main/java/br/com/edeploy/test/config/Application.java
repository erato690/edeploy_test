package br.com.edeploy.test.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("br.com.edeploy.test.*")
@EnableAutoConfiguration
@Import({WebMvcConfig.class})
public class Application {
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}